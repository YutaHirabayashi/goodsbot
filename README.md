# GoodsBot

## LineDevelopersに登録
https://developers.line.biz/console/channel/new?type=messaging-api&status=success

Webhook URLにURL(heroku)登録

応答メッセージの自動返信　OFF
https://manager.line.biz/account/@395tvvoh/autoresponse

## 環境構築

```
docker build -t goodsbot:0.1 .
docker-compose run web django-admin startproject config .
docker-compose run web django-admin startapp bot
docker-compose run web python manage.py makemigrations bot
docker-compose run web python manage.py migrate
docker-compose run web python manage.py createsuperuser

docker-compose run web up #Local動作確認

```

## 台詞テキストから文章生成
```
docker-compose run web python scripts/generate_sentence.py
```

## デプロイ
```
heroku create goodsbot
heroku addons:create heroku-postgresql:hobby-dev
heroku config:set SECRET_KEY='replace me with a generated secret key'
heroku config:set DISABLE_COLLECTSTATIC=1

git push heroku master:master
heroku run python manage.py createsuperuser

heroku ps:scale web=1 #start server
heroku open # see deployed website
heroku ps:scale web=0 #stop server