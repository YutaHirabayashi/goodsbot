import markovify
import MeCab

# Load file
text_file = open("bot/serif.txt", "r")
texts = text_file.readlines()

# Parse text using MeCab
parsed_text = ''
for text in texts:
    _text = MeCab.Tagger('-Owakati').parse(text)
    parsed_text += _text

# Build model
text_model = markovify.NewlineText(parsed_text, state_size=2, well_formed=False)

# Output
results = []
for _ in range(100000):
    sentence = text_model.make_sentence(tries=100)
    results.append(''.join(sentence.split()) + '\n')    # need to concatenate space-splitted text
with open("bot/serif_generated.txt", "w") as f:
    f.writelines(results)