# Pull base image
FROM python:3.6-slim

# Install psql so that "python manage.py dbshell" works
RUN apt-get update -qq && apt-get install -y postgresql-client

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 1

# Set work directory
WORKDIR /app

# install mecab for genarating sentence 
RUN apt install -y mecab
RUN apt install -y libmecab-dev
RUN apt install -y mecab-ipadic-utf8

# Install dependencies
COPY requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt

# local only
RUN pip install unidic-lite
RUN pip install mecab-python3
RUN pip install markovify

# Copy project
COPY . /app/
